﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetVogal
{
    class Program
    {
        static void Main(string[] args)
        {
           // string input = "aAbBABacfe";
            string input = "aACBABacfe";
            char vogal = firstChar(input);
            Console.WriteLine(vogal);
            Console.ReadKey();
        }


        public interface IStream
        {

        }
        public static char firstChar(string input)
        {
            char vogal = new char();
            char primeiraConsoante = new char();
            List<char> historico = new List<char>();
            string _input = input.ToUpper();
            char[] consoates = { 'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z' };
            bool consoanteAdicionada = false;

            for (int i = 0; i < _input.Length; i++)
            {
                if (!consoates.Contains(_input[i]))
                {
                    vogal = _input[i];
                }
                else
                {
                    if (consoanteAdicionada == false)
                    {
                        primeiraConsoante = _input[i];
                        consoanteAdicionada = true;
                    }
                }
                historico.Add(_input[i]);
            }


            return vogal;
        }

    }
}
